#!/bin/bash
set -e

# Build variables
export KBUILD_BUILD_USER=${KBUILD_BUILD_USER:-"user"}
export KBUILD_BUILD_HOST=${KBUILD_BUILD_HOST:-"custombuild"}
export ARCH=${ARCH:-"arm64"}
export CPUS=${CPUS:-$(($(getconf _NPROCESSORS_ONLN) - 1))}
export KERNEL_BRANCH=${KERNEL_BRANCH:-"icosa-v6.2"}
export CWD="$(realpath "$(dirname "${BASH_SOURCE[0]}")")"
export KERNEL_DIR="${CWD}/kernel"
export URL=${URL:-"https://gitlab.com/l4t-community/kernel/mainline/linux"}
export CROSS_COMPILE=${CROSS_COMPILE:-"aarch64-linux-gnu-"}

# Create compressed modules and update archive with correct permissions and ownership 
create_update_modules() {
	find "$1" -type d -exec chmod 755 {} \;
	find "$1" -type f -exec chmod 644 {} \;
	find "$1" -name "*.sh" -type f -exec chmod 755 {} \;
	fakeroot chown -R root:root "$1"
	tar -C "$1" -czvpf "$2" .
}

Prepare() {
	mkdir -p ${KERNEL_DIR}

	# Clone L4T kernel from l4t-community
	if [[ -z `ls -A ${KERNEL_DIR}/linux` ]]; then
		git clone -b "${KERNEL_BRANCH}" "${URL}" ${KERNEL_DIR}/linux/
	fi

	# Retrieve mkdtimg
	if [[ ! -e "${KERNEL_DIR}/mkdtimg" ]]; then
			wget https://android.googlesource.com/platform/system/libufdt/+archive/refs/heads/master/utils.tar.gz
			tar xvf utils.tar.gz
			cp src/mkdtboimg.py "${KERNEL_DIR}/mkdtimg"
			chmod a+x "${KERNEL_DIR}/mkdtimg"
			rm -rf utils.tar.gz tests src README.md
	fi
	export PATH="$(realpath ${KERNEL_DIR}):$PATH"
}

Build() {
	echo "Preparing Source and Creating Defconfig"

	cd ${KERNEL_DIR}/linux/
	make nintendo_switch_defconfig

	make -j${CPUS} Image
	make -j${CPUS} dtbs
	make -j${CPUS} modules

	mkimage -A arm64 -O linux -T kernel -C none -a 0x80200000 -e 0x80200000 -n "AZKRN-${KERNEL_BRANCH}" -d ${KERNEL_DIR}/linux/arch/arm64/boot/Image "${KERNEL_DIR}/uImage"

	mkdtimg create "${KERNEL_DIR}/nx-plat.dtimg" --page_size=1000 \
        ${KERNEL_DIR}/linux/arch/arm64/boot/dts/nvidia/tegra210-odin.dtb    --id="0x4F44494E" \
		${KERNEL_DIR}/linux/arch/arm64/boot/dts/nvidia/tegra210b01-odin.dtb --id=0x4F44494E --rev=0xb01 \
                ${KERNEL_DIR}/linux/arch/arm64/boot/dts/nvidia/tegra210b01-vali.dtb --id=0x56414C49 \
                ${KERNEL_DIR}/linux/arch/arm64/boot/dts/nvidia/tegra210b01-fric.dtb --id=0x46524947

	make modules_install INSTALL_MOD_PATH="${KERNEL_DIR}/modules/"
	make headers_install INSTALL_HDR_PATH="${KERNEL_DIR}/update/usr/"
}

PostConfig() {
	echo "Refresh permissions for kernel headers and Create compressed modules and headers"
	find "${KERNEL_DIR}/update/usr/include" -name *.install* -exec rm {} \;
	find "${KERNEL_DIR}/update/usr/include" -exec chmod 777 {} \;

	echo "Creating modules and update archives"
	create_update_modules "${KERNEL_DIR}/modules/lib/" "${KERNEL_DIR}/modules.tar.gz"
	create_update_modules "${KERNEL_DIR}/update/" "${KERNEL_DIR}/update.tar.gz"

	rm -rf "${KERNEL_DIR}/modules" "${KERNEL_DIR}/update/"
	echo "Done"
}

Prepare
Build
PostConfig
